import math
import sys
from Tkinter import *
from PIL import Image, ImageDraw, ImageFont, ImageOps

def Heu(desti):
    dic = {1:[-6,5],2:[-6,4],3:[-6,3], 4:[-5,1], 5:[-3,1], 6:[-1,0], 7:[0,0], 8:[3,-1], 9:[4,-1], 10:[5,-2], 11:[5,-3], 12:[5,-4], 13:[5,-5], 14:[8,3], 15:[3,4], 16:[2,5],
           17:[0,4], 18:[-3,3], 19:[-3,2], 20:[-3,0], 21:[-3,-1], 22:[-2,7], 23:[-2,6], 24:[-3,5], 25:[1,3], 26:[1,2], 27:[1,0], 28:[0,-2], 29:[0,-3], 30:[0,-4], 31:[0,-5],
           32:[-5,-1], 33:[-5,-3], 34:[-5,-4], 35:[-5,1], 36:[-3,1], 37:[0,0], 38:[-3,3], 39:[2,5]}
    auxi = dic[desti]
    auxi0 = auxi[0]
    auxi1 = auxi[1]
    dicti = {1:[],2:[],3:[],4:[],5:[],6:[],7:[],8:[],9:[],10:[],11:[],12:[],13:[],14:[],15:[],16:[],17:[],18:[],19:[],20:[],21:[],22:[],23:[],24:[],25:[],26:[],27:[],28:[],29:[],30:[],31:[],32:[],33:[],34:[],35:[],36:[],37:[],38:[],39:[]}
    for ele in dic:
        aux = dic[ele]
        aux0 = aux[0]
        aux1 = aux[1]
        distancia = math.sqrt((aux0-auxi0)*(aux0-auxi0) + (aux1-auxi1)*(aux1-auxi1))
        dicti[ele] = distancia*12
    return dicti

def T_temps():
        f = open('matriu_temps.txt','r')
        lista = []
        for line in f.readlines():
            aux = line
            lista.append(aux)
        f.close()
        defi = []
        trobat = False
        for i in range(len(lista)):
                auxi =[]
                nums = []
                for ele in lista[i]:
                        if(ele == '\t' or ele == '\n'): 
                                trobat = True
                                out = ''
                                for ele in auxi:
                                        out += ele
                                if(out != ''): nums.append(int(out))
                                auxi = []
                        if (trobat == False):
                                auxi.append(ele)
                        trobat = False
                if (i == 38): nums.append(0)
                defi.append(nums)

        dic = {1:[],2:[],3:[],4:[],5:[],6:[],7:[],8:[],9:[],10:[],11:[],12:[],13:[],14:[],15:[],16:[],17:[],18:[],19:[],20:[],21:[],22:[],23:[],24:[],25:[],26:[],27:[],28:[],29:[],30:[],31:[],32:[],33:[],34:[],35:[],36:[],37:[],38:[],39:[]}

        for i in range(len(defi)):
                ele = defi[i]
                for index in range(len(ele)):
                        if(ele[index] != 0):
                                lista = [index+1]
                                lista.append(ele[index])
                                dic[i+1].append(lista)
        return dic

def T_noms(nombre):
        dic = {1:['GARE_VAISE','D'], 2:['VALMY' ,'D'], 3:['GORGE_DE_LOUP','D'], 4:['VIEUX_LYON' ,'D'], 5:['BELLECOUR' ,'D'] ,6:['GUILLOTIERE' ,'D'] ,7:['SAXE-GAMBETTA' ,'D']
               ,8:['MONPLAISIR-LUMIERE','D'],9:['GRANGE-BLANCHE','D'] ,10:['LAENNEC','D'] ,11:['MERMOZ-PINEL','D'] ,12:['PARILLY','D'] ,13:['GARE_DE_VENISSEUX','D'] ,
               14:['LAURENT_BONNEVAY','A'] ,15:['REPUBLIQUE','A'] ,16:['CHARPENNES','A'] ,17:['MASSENA','A'] ,18:['HOTEL_DE_VILLE','C'] ,19:['CORDELIERS','A']
               ,20:['AMPERE','A'],21:['PERRACHE','A'],22:['CUIRE','C'] ,23:['HENON','C'] ,24:['CROIX-ROUSSE','C'],25:['BROTTEAUX','B'] ,26:['PART-DIEU','B']
               ,27:['PLACE_GUICHAR','B'] ,28:['JEAN_MACE','B'],29:['PLACE_JEAN_JAURES','B'] ,30:['DEBOURG','B'] ,31:['STADE_DE_GERLAND','B'] ,32:['FOURVIERE','E']
               ,33:['NINIMES','E'] ,34:['ST JUST','E'] ,35:['VIEUX_LYON','E'] ,36:['BELLECOUR','A'] ,37:['SAXE-GAMBETTA','B'] ,38:['HOTEL_DE_VILLE','A']
               ,39:['CHARPENNES','B']}
        for x in dic:
            aux = dic[x]
            if(aux[0] == nombre):return x

            
class Cami(object):
    def __init__(self):
        self.cami = []
        self.acu = 0
        self.heu = 0
        self.parades = 0
        self.transbords = 0

class Llistes(object):
    def __init__ (self):
        self.llista = []

    def Estrella(self, origen, objectiu, d, c):
        self.llista = [origen]
        trobat = False
        while(trobat == False):
            node = self.llista[0]
            possi = d[node.cami[0]]
            for i in possi:
                a = Cami()
                a.cami = [i[0]]
                for ele in node.cami:
                    a.cami.append(ele)
                a.acu = node.acu
                a.acu += i[1]
                if(a.cami[0] == objectiu.cami[0]): a.heu = 0
                else: a.heu = c[a.cami[0]]
                self.llista.append(a)
            self.llista.remove(node)
            aux = self.llista
            for ele in aux:
                auxi = []
                for nombre in ele.cami:
                    for i in range(len(auxi)):
                        if(auxi[i] == nombre): aux.remove(ele)
                    auxi.append(nombre)
            aux2 = []
            min_c = Cami()
            while(len(aux)!=0):
                min = 1000
                for ele in aux:
                     if((ele.acu+ele.heu)<min):
                        min_c = ele
                        min = ele.acu+ele.heu
                aux.remove(min_c)
                aux2.append(min_c)
            self.llista = aux2
            final = self.llista[0]
            if(final.cami[0] == objectiu.cami[0]):
                trobat = True
                return final
            
    def Estrella2(self, origen, objectiu, d, c):
        origen.parades = 1
        origen.heu = 0
        self.llista = [origen]
        trobat = False
        while(trobat == False):
            node = self.llista[0]
            possi = d[node.cami[0]]
            for i in possi:
                a = Cami()
                a.cami = [i[0]]
                for ele in node.cami:
                    a.cami.append(ele)
                a.acu = node.acu
                a.acu += i[1]
                a.parades = len(a.cami)
                if(a.cami[0] == objectiu.cami[0]): a.heu = 0
                else: a.heu = c[a.cami[0]]
                self.llista.append(a)
            self.llista.remove(node)
            aux = self.llista
            for ele in aux:
                auxi = []
                for nombre in ele.cami:
                    for i in range(len(auxi)):
                        if(auxi[i] == nombre): aux.remove(ele)
                    auxi.append(nombre)
            aux2 = []
            min_c = Cami()
            while(len(aux)!=0):
                min = 1000
                for ele in aux:
                     if((ele.parades)<min):
                        min_c = ele
                        min = ele.parades
                aux.remove(min_c)
                aux2.append(min_c)
            self.llista = aux2
            final = self.llista[0]
            if(final.cami[0] == objectiu.cami[0]):
                trobat = True
                return final
            
    def Estrella3(self, origen, objectiu, d, c, t):
        origen.heu = 0
        self.llista = [origen]
        trobat = False
        while(trobat == False):
            node = self.llista[0]
            possi = d[node.cami[0]]
            for i in possi:
                a = Cami()
                a.cami = [i[0]]
                for ele in node.cami:
                    a.cami.append(ele)
                a.acu = node.acu
                a.acu += i[1]
                linea_a = t[node.cami[0]]
                linea_a = linea_a[1]
                linea_b = t[a.cami[0]]
                linea_b = linea_b[1]
                if(linea_a != linea_b): a.transbords += 1
                if(a.cami[0] == objectiu.cami[0]): a.heu = 0
                else: a.heu = c[a.cami[0]]
                self.llista.append(a)
            self.llista.remove(node)
            aux = self.llista
            auxi = []
            for a in aux:
                trobat1 = False
                for ele in a.cami:
                    acu = 0
                    for i in range(len(a.cami)):
                        if(ele == a.cami[i] and trobat1 == False):
                            acu += 1
                            if(acu == 2):
                                auxi.append(a)
                                trobat1 = True
            for x in range(len(auxi)): aux.remove(auxi[x])
            aux2 = []
            min_c = Cami()
            while(len(aux)!=0):
                min = 1000
                for ele in aux:
                     if((ele.transbords)<min):
                        min_c = ele
                        min = ele.transbords
                aux.remove(min_c)
                aux2.append(min_c)
            self.llista = aux2
            final = self.llista[0]
            if(final.cami[0] == objectiu.cami[0]):
                trobat = True
                return final

def Coordenades(llista):
    dic = {1:[180,135],2:[180,180],3:[180,225],4:[225,315],5:[315,315],6:[405,360],7:[450,360],8:[585,405],9:[630,405], 10:[675,450],11:[675,495],12:[675,540],13:[675,585],14:[810,225],15:[585,180],16:[545,135],17:[450,180]  
        ,18:[315,225],19:[315,270],20:[315,360],21:[315,405],22:[360,45],23:[360,90],24:[315,135],25:[495,225], 26:[495,270],27:[495,360],28:[450,450],29:[450,495],30:[450,540],31:[450,585],32:[225,405],33:[225,495],
           34:[225,540],35:[225,315],36:[315,315],37:[450,360],38:[315,225],39:[545,135]}
    aux = []
    for ele in llista:
        aux.append(dic[ele])
    return aux

def Definitiu(origen, desti, opcio):
    ori = Cami()
    des = Cami()
    lista = Llistes()
    ori.cami = [T_noms(origen)]
    ori.acu = 0
    des.cami = [T_noms(desti)]
    des.acu = 0
    des.heu = 0
    heu = Heu(des.cami[0])
    ori.heu = heu[ori.cami[0]]
    dic = T_temps()
    if (opcio == 'TEMPS'):
        a = lista.Estrella(ori,des,dic,heu)
        return a
    if (opcio == 'PARADES'):
        a = lista.Estrella2(ori,des,dic,heu)
        return a
    if (opcio == 'TRANSBORDS'):
        print "transbords"
        t = {1:['GARE_VAISE','D'], 2:['VALMY' ,'D'], 3:['GORGE_DE_LOUP','D'], 4:['VIEUX_LYON' ,'D'], 5:['BELLECOUR' ,'D'] ,6:['GUILLOTIERE' ,'D'] ,7:['SAXE-GAMBETTA' ,'D']
               ,8:['MONPLAISIR-LUMIERE','D'],9:['GRANGE-BLANCHE','D'] ,10:['LAENNEC','D'] ,11:['MERMOZ-PINEL','D'] ,12:['PARILLY','D'] ,13:['GARE_DE_VENISSEUX','D'] ,
               14:['LAURENT_BONNEVAY','A'] ,15:['REPUBLIQUE','A'] ,16:['CHARPENNES','A'] ,17:['MASSENA','A'] ,18:['HOTEL_DE_VILLE','C'] ,19:['CORDELIERS','A']
               ,20:['AMPERE','A'],21:['PERRACHE','A'],22:['CUIRE','C'] ,23:['HENON','C'] ,24:['CROIX-ROUSSE','C'],25:['BROTTEAUX','B'] ,26:['PART-DIEU','B']
               ,27:['PLACE_GUICHAR','B'] ,28:['JEAN_MACE','B'],29:['PLACE_JEAN_JAURES','B'] ,30:['DEBOURG','B'] ,31:['STADE_DE_GERLAND','B'] ,32:['FOURVIERE','E']
               ,33:['NINIMES','E'] ,34:['ST JUST','E'] ,35:['VIEUX_LYON','E'] ,36:['BELLECOUR','A'] ,37:['SAXE-GAMBETTA','B'] ,38:['HOTEL_DE_VILLE','A']
               ,39:['CHARPENNES','B']}
        a = lista.Estrella3(ori,des,dic,heu,t)
        return a
    if (opcio == 'PEU'):
        return 0
    

PIL_Version = Image.VERSION

def newwin2():
    origen=v.get()
    destino=v2.get()
    opcio =v3.get()
    a=Definitiu(origen,destino,opcio)
    im = Image.open('Graellad.jpg')
    draw = ImageDraw.Draw(im)
    if (a != 0):
        cor = Coordenades(a.cami)
        for i in range(len(cor)):
            if(i <  len(cor)-1):
                aux0 = cor[i]
                aux1 = cor[i+1]
                draw.line([(aux0[0],aux0[1]),(aux1[0],aux1[1])],fill=(128,0,128),width=10)
            else:
                aux0 = cor[i-1]
                aux1 = cor[i]
                draw.line([(aux0[0],aux0[1]),(aux1[0],aux1[1])],fill=(128,0,128),width=10)
        im.show()
        raiz1=Tk()
        raiz1.title("Temps")
        texto="Minuts en metro:\n"+str(a.acu)
        win= Label(raiz1, text=texto)
        win.grid(row=2, column=2)

    else:
        ori = T_noms(origen)
        des = T_noms(destino)
        heu = Heu(des)
        temps = heu[ori]
        aux0 = Coordenades([ori])[0]
        aux1 = Coordenades([des])[0]
        draw.line([(aux0[0],aux0[1]),(aux1[0],aux1[1])],fill=(128,0,128),width=10)
        im.show()
        raiz2=Tk()
        raiz2.title("Temps")
        texto="Minuts a peu:\n"+str(temps)
        win= Label(raiz2, text=texto)
        win.grid(row=2, column=2)    
    

raiz=Tk()
raiz.title("METRO LYON")
texto="ORIGEN:\n"
win= Label(raiz, text=texto)
win.grid(row=2, column=2)
v = StringVar()
frame = Frame()
text = Entry(frame, textvariable=v )
text.grid(row=3, column=2)
frame.grid(row=4, column=2)
texto2="DESTI:\n"
win2= Label(raiz, text=texto2, width=30)
win2.grid(row=5, column=2)
v2 = StringVar()
frame2 = Frame()
text2 = Entry(frame2, textvariable=v2 )
text2.grid(row=6, column=2)
frame2.grid(row=7, column=2)
texto3="OPCIO:\n"
win3= Label(raiz, text=texto3, width=30)
win3.grid(row=10, column=2)
v3 = StringVar()
frame3 = Frame()
text3 = Entry(frame3, textvariable=v3 )
text3.grid(row=12, column=2)
frame3.grid(row=14, column=2)
calcular = Button(raiz, text="VAMOS ALLA!", command=newwin2, width=10)
calcular.grid(row=20, column=2)
raiz.mainloop()
